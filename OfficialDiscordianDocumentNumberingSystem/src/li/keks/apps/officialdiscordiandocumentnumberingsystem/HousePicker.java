package li.keks.apps.officialdiscordiandocumentnumberingsystem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;

public class HousePicker extends Activity implements OnClickListener {

    private HouseListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_picker);
        // Show the Up button in the action bar.
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        ExpandableListView houseList = (ExpandableListView) findViewById(R.id.houseList);
        houseList.setOnGroupClickListener(new DiscordianOnGroupClickListener());
        listAdapter = new HouseListAdapter();
        houseList.setAdapter(listAdapter);

        Button button = (Button) findViewById(R.id.ok);
        button.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent data = this.getIntent();
        String text = listAdapter.getHouse() + " " + listAdapter.getSubdivision();
        data.putExtra("house", text);
        data.putExtra("houseid", listAdapter.getHouseId());
        data.putExtra("subdivisionid", listAdapter.getSubdivisionId());
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        finish();
    }

}
