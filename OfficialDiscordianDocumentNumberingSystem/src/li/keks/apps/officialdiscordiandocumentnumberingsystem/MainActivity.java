package li.keks.apps.officialdiscordiandocumentnumberingsystem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;


public class MainActivity extends Activity implements OnClickListener,
        DatePickerDialog.OnDateSetListener {

    private int year;
    private int monthOfYear;
    private int dayOfMonth;
    private int dyear;
    private int dmonthOfYear;
    private int ddayOfMonth;
    private String author;
    private String recipient;

    private String[][] crazy_map = new String[][]{
            {"I", "II", "III", "IV", "V"},
            {"(a)", "(b)", "(c)", "(d)", "(e)"},
            {"1", "2", "3", "4", "5"},
            {"i", "ii", "iii", "iv", "v"},
            {"Chs", "Dsc", "Cfn", "Bcy", "Afm"}
    };

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.pickAuthor);
        button.setOnClickListener(this);

        button = (Button) findViewById(R.id.pickRecipient);
        button.setOnClickListener(this);

        /*
        button = (Button)findViewById(R.id.refresh);
        button.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                generateODDN();
            }
        });
        */
        button = (Button)findViewById(R.id.copy);
        if(android.os.Build.VERSION.SDK_INT < 14)
        {
            button.setVisibility(View.INVISIBLE);
        } else {
            button.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    ClipboardManager clipboard = (ClipboardManager)
                            getSystemService(Context.CLIPBOARD_SERVICE);
                    TextView oddn = (TextView) findViewById(R.id.odd);
                    ClipData clip = ClipData.newPlainText(oddn.getText(), oddn.getText());
                    clipboard.setPrimaryClip(clip);
                    if (getApplicationContext() != null) {
                        int duration = Toast.LENGTH_SHORT;
                        Toast toast = Toast.makeText(getApplicationContext(), "Copy to clipboard successful", duration);
                        toast.show();
                    }
                }
            });
        }

        button = (Button) findViewById(R.id.datePicker);
        button.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        generateODDN();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    public void showDatePickerDialog() {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this, year, month, day);
        datePickerDialog.show();
    }

    public void onClick(View v) {
        Intent intent = new Intent(this, HousePicker.class);
        if (v.getId() == R.id.pickAuthor) {
            startActivityForResult(intent, R.id.pickAuthor);
        }
        if (v.getId() == R.id.pickRecipient) {
            startActivityForResult(intent, R.id.pickRecipient);
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
            Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == R.id.pickAuthor) {
                this.author = crazy_map[0][data.getIntExtra("houseid", 0)] +
                        crazy_map[1][data.getIntExtra("subdivisionid", 0)];
                TextView text = (TextView) findViewById(R.id.from);
                text.setText(data.getStringExtra("house"));
            }
            if (requestCode == R.id.pickRecipient) {
                this.recipient = crazy_map[2][data.getIntExtra("houseid", 0)] + "," +
                        crazy_map[3][data.getIntExtra("subdivisionid", 0)];
                TextView text = (TextView) findViewById(R.id.to);
                text.setText(data.getStringExtra("house"));
            }
        }
        generateODDN();
    }

    private void generateODDN() {
        TextView output = (TextView)findViewById(R.id.odd);
        String oddn = "ODD# " + this.author + "/" + this.recipient + ";"
                + this.ddayOfMonth + crazy_map[4][this.dmonthOfYear] + this.dyear;
        output.setText(oddn);
    }

    public void calculateDiscordianDate() {
        this.dyear = this.year + 1166;
        this.ddayOfMonth = this.dayOfMonth;
        switch (this.monthOfYear) {
            case Calendar.JANUARY:
                this.dmonthOfYear = 0;
                break;
            case Calendar.FEBRUARY:
                if (this.dayOfMonth == 29) {
                    this.dmonthOfYear = -1;
                    this.ddayOfMonth = -1;
                } else {
                    this.dmonthOfYear = 0;
                    this.ddayOfMonth += 31;
                }
                break;
            case Calendar.MARCH:
                this.ddayOfMonth += 59;
                if (this.ddayOfMonth <= 73) {
                    this.dmonthOfYear = 0;
                } else {
                    this.ddayOfMonth -= 73;
                    this.dmonthOfYear = 1;
                }
                break;
            case Calendar.APRIL:
                this.ddayOfMonth += 17;
                this.dmonthOfYear = 1;
                break;
            case Calendar.MAY:
                this.ddayOfMonth += 47;
                if (this.ddayOfMonth <= 73) {
                    this.dmonthOfYear = 1;
                } else {
                    this.ddayOfMonth -= 73;
                    this.dmonthOfYear = 2;
                }
                break;
            case Calendar.JUNE:
                this.ddayOfMonth += 5;
                this.dmonthOfYear = 2;
                break;
            case Calendar.JULY:
                this.ddayOfMonth += 35;
                this.dmonthOfYear = 2;
                break;
            case Calendar.AUGUST:
                this.ddayOfMonth += 66;
                if (this.ddayOfMonth <= 73) {
                    this.dmonthOfYear = 2;
                } else {
                    this.ddayOfMonth -= 73;
                    this.dmonthOfYear = 3;
                }
                break;
            case Calendar.SEPTEMBER:
                this.ddayOfMonth += 24;
                this.dmonthOfYear = 3;
                break;
            case Calendar.OCTOBER:
                this.ddayOfMonth += 54;
                if (this.ddayOfMonth <= 73) {
                    this.dmonthOfYear = 3;
                } else {
                    this.ddayOfMonth -= 73;
                    this.dmonthOfYear = 4;
                }
                break;
            case Calendar.NOVEMBER:
                this.ddayOfMonth += 12;
                this.dmonthOfYear = 4;
                break;
            case Calendar.DECEMBER:
                this.dayOfMonth += 42;
                this.dmonthOfYear = 4;
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        this.year = year;
        this.monthOfYear = monthOfYear;
        this.dayOfMonth = dayOfMonth;
        calculateDiscordianDate();
        generateODDN();
    }
}
