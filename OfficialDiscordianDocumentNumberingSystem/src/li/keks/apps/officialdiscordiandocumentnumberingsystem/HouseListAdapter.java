package li.keks.apps.officialdiscordiandocumentnumberingsystem;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class HouseListAdapter implements ExpandableListAdapter,
        OnClickListener {

    private static final String[] HOUSES = {
            "I. THE OUT HOUSE",
            "II. THE HOUSE OF THE RISING COLLAPSE",
            "III. THE HOUSE OF THE RISING HODGE",
            "IV. THE HOUSE OF THE RISING PODGE",
            "V. THE HOUSE OF APOSTLES OF ERIS"};

    private static final String[][] SUBDIVISIONS = {
            {"A. Miscellaneous Avatars", "B. The Fifth Column",
                    "POEE =POPES= everywhere", "Drawer \"O\" for OUT OF FILE",
                    "E. Lost Documents and Forgotten Truths"},
            {"A. The Breeze of Wisdom and/or The Wind of Insanity",
                    "B. The Breeze of Integrity and/or The Wind of Arrogance",
                    "C. The Breeze of Beauty and/or The Wind of Outrages",
                    "D. The Breeze of Love and/or The Wind of Bombast",
                    "E. The Breeze of Laughter and/or The Wind of Bullshit"},
            {
                    "A. The Bureau of Erisian Archives",
                    "B. The Bureau of The POEE Epistolary, and The Division of Dogmas",
                    "C. The Bureau of Symbols, Emblems, Certificates and Such",
                    "D. The Bureau of Eristic Affairs, and The Administry for The Unenlightened Eristic Horde",
                    "E. The Bureau of Aneristic Affairs, and the Administry for the Orders of Discordia"},
            {"A. Office of My High Reverence, The Polyfather",
                    "B. Council of POEE Prists",
                    "C. The LEGINO OF DYNAMIC DISCORD", "D. Eristic Avatars",
                    "E. Aneristic Avatars"},
            {"A. The Five Apostles of ERIS",
                    "B. The Golden Apple Corps (KSC)",
                    "C. Episkoposes of The Discordian Society",
                    "D. POEE Cabal Priests",
                    "E. Saints, Erisian Avatars, and Like Personages"}};

    private int idHouse;
    private int idSubdivision;

    public HouseListAdapter() {
        // TODO Auto-generated constructor stub
    }

    public boolean areAllItemsEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    public Object getChild(int groupPosition, int childPosition) {
        return SUBDIVISIONS[groupPosition % 5][childPosition % 5];
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater infl = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RadioGroup group = (RadioGroup) infl.inflate(
                R.layout.subdivision_group, null);
        ((RadioButton) group.getChildAt(0)).setText((String) getChild(
                groupPosition, 0));
        group.getChildAt(0).setOnClickListener(this);
        group.getChildAt(0).setTag(R.id.tag_subdivision, 0);
        group.getChildAt(0).setTag(R.id.tag_house, groupPosition);

        ((RadioButton) group.getChildAt(1)).setText((String) getChild(
                groupPosition, 1));
        group.getChildAt(1).setOnClickListener(this);
        group.getChildAt(1).setTag(R.id.tag_subdivision, 1);
        group.getChildAt(1).setTag(R.id.tag_house, groupPosition);

        ((RadioButton) group.getChildAt(2)).setText((String) getChild(
                groupPosition, 2));
        group.getChildAt(2).setOnClickListener(this);
        group.getChildAt(2).setTag(R.id.tag_subdivision, 2);
        group.getChildAt(2).setTag(R.id.tag_house, groupPosition);

        ((RadioButton) group.getChildAt(3)).setText((String) getChild(
                groupPosition, 3));
        group.getChildAt(3).setOnClickListener(this);
        group.getChildAt(3).setTag(R.id.tag_subdivision, 3);
        group.getChildAt(3).setTag(R.id.tag_house, groupPosition);

        ((RadioButton) group.getChildAt(4)).setText((String) getChild(
                groupPosition, 4));
        group.getChildAt(4).setOnClickListener(this);
        group.getChildAt(4).setTag(R.id.tag_subdivision, 4);
        group.getChildAt(4).setTag(R.id.tag_house, groupPosition);

        return group;
    }

    public int getChildrenCount(int groupPosition) {
        // always five
        return 1;
    }

    public long getCombinedChildId(long groupId, long childId) {
        return groupId * 5 + childId;
    }

    public long getCombinedGroupId(long groupId) {
        return groupId;
    }

    public Object getGroup(int groupPosition) {
        return HOUSES[groupPosition];
    }

    public int getGroupCount() {
        return 5;
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        LayoutInflater infl = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (isExpanded) {
            LinearLayout child = (LinearLayout) infl.inflate(
                    R.layout.group_expanded, null);
            ((TextView) child.getChildAt(0))
                    .setText((String) getGroup(groupPosition));
            return child;
        } else {
            LinearLayout child = (LinearLayout) infl.inflate(
                    R.layout.group_collapsed, null);
            ((TextView) child.getChildAt(0))
                    .setText((String) getGroup(groupPosition));
            return child;
        }
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }

    public void onGroupCollapsed(int groupPosition) {
        // TODO Auto-generated method stub

    }

    public void onGroupExpanded(int groupPosition) {
        // TODO Auto-generated method stub

    }

    public void registerDataSetObserver(DataSetObserver observer) {
        // TODO Auto-generated method stub

    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        // TODO Auto-generated method stub

    }

    public int getHouseId() {
        return idHouse;
    }

    public String getHouse() {
        return HOUSES[idHouse];
    }

    public int getSubdivisionId() {
        return idSubdivision;
    }

    public String getSubdivision() {
        return SUBDIVISIONS[idHouse][idSubdivision];
    }

    public void onClick(View view) {
        idSubdivision = ((Integer) view.getTag(R.id.tag_subdivision));
        idHouse = ((Integer) view.getTag(R.id.tag_house));
    }
}
