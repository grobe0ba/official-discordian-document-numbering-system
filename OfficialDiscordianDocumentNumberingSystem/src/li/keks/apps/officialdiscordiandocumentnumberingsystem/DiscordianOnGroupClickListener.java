package li.keks.apps.officialdiscordiandocumentnumberingsystem;

import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;


public class DiscordianOnGroupClickListener implements OnGroupClickListener {

    public DiscordianOnGroupClickListener() {
    }

    public boolean onGroupClick(ExpandableListView parent, View groupView, int groupPosition,
                                long groupId) {
        for (int pos = 0; pos < parent.getChildCount(); pos++) {
            if (pos == groupPosition) {
                if (parent.isGroupExpanded(groupPosition)) {
                    parent.collapseGroup(groupPosition);
                } else {
                    parent.expandGroup(groupPosition);
                }
            } else {
                parent.collapseGroup(pos);
            }
        }
        return true;
    }

}
